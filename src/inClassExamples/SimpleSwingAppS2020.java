package inClassExamples;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimpleSwingAppS2020 {

	private JTextField inputField;
	private JTextArea jta;

	private final class InputListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String input = inputField.getText();
			jta.append(input);
			jta.append("\n");
		}
	}

	public static void main(String[] args) {
		new SimpleSwingAppS2020();
	}
	
	public SimpleSwingAppS2020() {
		JFrame frame = new JFrame("My simple app");
		Container cont = frame.getContentPane();
		cont.setLayout(new BorderLayout());
		
		inputField = new JTextField(20);
		InputListener il = new InputListener();
		inputField.addActionListener(il);
		JButton button = new JButton("append stuff");
		
		//Box topBox = Box.createHorizontalBox();
		JPanel topPanel = new JPanel(new FlowLayout());
		topPanel.add(inputField);
		topPanel.add(button);
		 
		cont.add(topPanel, BorderLayout.NORTH);
		
		jta = new JTextArea(10, 60);
		jta.setEditable(false);
		cont.add(jta,BorderLayout.CENTER);

		button.addActionListener(il);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jta.append("Blah");
			}
		});
		
		frame.pack();
		frame.setSize(800, 600);
		frame.setVisible(true);
	}

}
